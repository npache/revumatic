import tuimatic

from modules import (
    screen,
    ui,
)

class MenuScreen(screen.BaseScreen):
    def init_ui(self):
        self.widgets.menu = tuimatic.ListBox()
        actions = []
        self.add_actions(actions)
        self.action_tracker.add_actions(self.widgets.menu, actions)

        self.widgets.help = tuimatic.Text('')
        self.widgets.help_frame = tuimatic.LineBox(self.widgets.help)

        title = self.get_title()
        if title:
            self.app.set_header(('header', ' {} '.format(title)))
        self.widgets.pile = tuimatic.Pile([self.widgets.menu])
        return self.widgets.pile

    def load_data(self):
        self.walker = ui.SmartListWalker(self.load_keys(),
                                         self.app.focus_map, self.app.passive_map,
                                         formatter=self.format_item)
        self.widgets.menu.body = self.walker
        tuimatic.connect_signal(self.walker, 'focus-changed', self.show_help)
        self.show_help()

    @property
    def current_key(self):
        """Returns the currently focused key. Can be None."""
        try:
            return self.walker.get_focused_object()
        except (IndexError, TypeError):
            # empty listbox
            return None

    def update_current_item(self):
        self.walker.reformat_focused_text()

    def delete_current_item(self):
        del self.walker[self.walker.focus]

    def add_key(self, key):
        self.walker.append(key)

    def show_help(self):
        text = None
        key = self.current_key
        if key is not None:
            text = self.get_help(key)
        if text:
            self.widgets.help.set_text(text)
            if len(self.widgets.pile.contents) == 1:
                self.widgets.pile.contents.append((self.widgets.help_frame, ('pack', None)))
        else:
            if len(self.widgets.pile.contents) == 2:
                del self.widgets.pile.contents[1]

    def add_actions(self, actions):
        return

    def get_title(self):
        return None

    def load_keys(self):
        return ()

    def format_item(self, key):
        return key

    def get_help(self, key):
        return None
