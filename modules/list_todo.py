import re
import tuimatic

from modules.utils import lab
from modules import (
    git,
    gitlab,
    list_base,
    mr,
    packages,
    screen,
    ui,
)

class Screen(list_base.BaseApprovalsScreen):
    command = 'todo'
    help = 'show the TODO list'

    name = 'TODO'
    config_section = 'todo'
    config_section_display_name = 'TODO list view options'
    default_sort = 'reverse created'
    available_sort = ['created']
    display_date = 'created'
    todo_action = False

    def add_actions(self, actions):
        actions.append(
            { 'name': 'mark-done', 'prio': 3, 'help': 'mark as done',
              'confirm': 'Do you want to mark this item as done?' })

    def fetch_list(self):
        todos = lab.todos(type='MergeRequest')

        nl_re = re.compile('\n+')
        result = []
        seen = {}
        for t in todos:
            act_data = {}
            if t['action_name'] in ('directly_addressed', 'mentioned'):
                # these two actions are the same thing, see
                # https://gitlab.com/gitlab-org/gitlab/-/issues/119232
                act_data['_priority'] = 7
                act_data['who'] = t['author']['name']
                act_data['action'] = 'mentioned you on'
                act_data['body'] = nl_re.sub(' ', t['body'])
            elif t['action_name'] == 'review_requested':
                act_data['_priority'] = 3
                act_data['action'] = 'review of'
            elif t['action_name'] == 'approval_required':
                act_data['_priority'] = 4
                act_data['action'] = 'requested approval of'
            elif t['action_name'] == 'assigned':
                act_data['_priority'] = 5
                act_data['who'] = t['author']['name']
                act_data['action'] = 'assigned you'
            elif t['action_name'] == 'build_failed':
                act_data['_priority'] = 6
                act_data['action'] = 'failed build for'
            elif t['action_name'] == 'marked':
                act_data['_priority'] = 2
                act_data['action'] = 'todo'
            else:
                # fallback for unknown actions
                act_data['_priority'] = 1
                act_data['action'] = t['action_name']
            act_data['created'] = gitlab.parse_datetime(t['created_at'])

            main_mr = seen.get(t['target']['id'])
            if main_mr:
                # this MR is already on the TODO list; check whether we
                # want to replace it with the new data
                if main_mr['_priority'] < act_data['_priority']:
                    main_mr.pop('who', None)
                    main_mr.pop('body', None)
                    main_mr.update(act_data)
                main_mr.setdefault('other_objs', []).append(t)
            else:
                pkg = packages.find(t['project']['path_with_namespace'], exclude_hidden=True)
                mr = self.parse_mr(pkg, t, t['target'], t['project']['name'])
                mr.update(act_data)
                result.append(mr)
                seen[t['target']['id']] = mr

        # Remove author for the 'directly_addressed' and 'mentioned' types.
        # This is a bit of hack, we abuse the fact that those types are the
        # only ones setting 'body'.
        for mr in result:
            if mr.get('body'):
                mr['author'] = None
        return result

    def update_actions(self):
        self.action_tracker.set_action_enabled('mark-done', self.mr_count > 0)
        super().update_actions()

    def mark_done(self):
        todo = self.walker.get_focused_object()
        popup = self.app.wait_popup('Working...')
        todo['obj'].mark_as_done()
        for obj in todo.get('other_objs', []):
            obj.mark_as_done()
        popup.stop()
        self.del_mr(self.widgets.mr_list.focus_position)
        self.update_actions()

    def action(self, what, widget, size):
        if super().action(what, widget, size):
            return
        if what == 'mark-done':
            self.mark_done()
